Age and market capitalization drive large price variations of cryptocurrencies
===============================================================================================

This repository contains the data and code necessary to reproduce all results presented in:

- Arthur A. B. Pessa, Matjaž Perc, Haroldo V. Ribeiro, `Age and market capitalization drive large price variations of cryptocurrencies <https://doi.org/10.1038/s41598-023-30431-3>`_,  Scientific Reports (Accepted, 2023). `arXiv:2302.12319 <https://arxiv.org/abs/2302.12319>`_

.. code-block:: bibtex
    
   @article{pessa2023age,
    title         = {Age and market capitalization drive large price variations of cryptocurrencies}, 
    author        = {Arthur A. B. Pessa and Matjaz Perc and Haroldo V. Ribeiro},
    journal       = {Scientific Reports},
    volume        = {13},
    pages         = {30431},
    year          = {2023},
    doi           = {10.1038/s41598-023-30431-3},
   }
